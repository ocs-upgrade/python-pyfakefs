%global package_name pyfakefs

Summary:        pyfakefs implements a fake file system that mocks the Python file system modules.
Name:           python-%{package_name}
Version:        5.7.1
Release:        1%{?dist}
License:        Apache-2.0
URL:            https://pypi.org/project/pyfakefs/
Source0:        https://pypi.io/packages/source/p/%{package_name}/%{package_name}-%{version}.tar.gz
BuildArch:      noarch


%description
pyfakefs implements a fake file system that mocks the Python file system
modules.Using pyfakefs, your tests operate on a fake file system in memory without
touching the real disk. The software under test requires no modification to
work with pyfakefs.

%package -n python3-%{package_name}
Summary:        %{summary}
Provides:       python3-%{package_name}

BuildRequires:  git-core python3-devel python3-setuptools
Requires:       python3-pytest >= 2.8.6

%description -n python3-%{package_name}
pyfakefs implements a fake file system that mocks the Python file system
modules.Using pyfakefs, your tests operate on a fake file system in memory without
touching the real disk. The software under test requires no modification to
work with pyfakefs.

%prep
%autosetup -n %{package_name}-%{version} -S git
rm -f {,test-}requirements.txt

%build
%py3_build

%install
%py3_install

%files -n python3-%{package_name}
%license COPYING
%doc README.md
%{python3_sitelib}/%{package_name}
%{python3_sitelib}/*.egg-info

%changelog
* Tue Oct 15 2024 Upgrade Robot <upbot@opencloudos.tech> - 5.7.1-1
- Upgrade to version 5.7.1

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.2.4-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.2.4-2
- Rebuilt for loongarch release

* Sat Oct 07 2023 Wang Guodong <gordonwwang@tencent.com> - 5.2.4-1
- Upgrade to version 5.2.4

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.2.2-4
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.2.2-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.2.2-2
- Rebuilt for OpenCloudOS Stream 23.05

* Wed Apr 19 2023 Feng Weiyao <wynnfeng@tencent.com> - 5.2.2-1
- initial build
